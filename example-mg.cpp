#include "includes/SolverConfig.hpp"
#include <libmesh/elem.h>
#include <libmesh/equation_systems.h>
#include <libmesh/exodusII_io.h>
#include <libmesh/libmesh.h>
#include <libmesh/mesh.h>
#include <libmesh/mesh_base.h>
#include <libmesh/mesh_generation.h>
#include <libmesh/mesh_refinement.h>
#include <libmesh/nonlinear_implicit_system.h>
#include <libmesh/petsc_matrix.h>
#include <libmesh/petsc_nonlinear_solver.h>
#include <libmesh/petsc_vector.h>

#include <chrono>
#include <fstream>
#include <utopia.hpp>
#include <utopia_fe.hpp>

#include "includes/Bratu.hpp"
#include "includes/DeSaintVenant.hpp"
#include "includes/LibMopiaInputParams.hpp"
#include "includes/LinearElasticity.hpp"
#include "includes/LinearElasticity3D.hpp"
#include "includes/ML_hierarchy.hpp"
#include "includes/Minsurface.hpp"
#include "includes/Neohookean.hpp"
#include "includes/Poisson.hpp"

// TODO list
// redo structure of classes
// fix how BC and handled in Opt.Problem interface class
// remove bc conditions from the transfer
// refactor other classes

int main(int argc, char **argv) {
  utopia::Utopia::Init(argc, argv);
  utopia::InputParameters params;
  params.init(argc, argv);

  // check if input file exist
  std::string input;

  if (argc >= 2) {
    input = argv[1];
  } else {
    input = "../inputs/InputLinearElasticity.in";
  }

  GetPot infile(input);
  LibMopiaInputParameters my_params;
  my_params.readParameters(infile);

  {
    using namespace libMesh;
    using namespace utopia;

    // Initialize libMesh and any dependent libraries
    LibMeshInit init(argc, argv, PETSC_COMM_WORLD);

    /////////////////////////////////////////////////////////////////////////////////
    // generated mesh
    auto mesh_coarse =
        std::make_shared<libMesh::Mesh>(init.comm(), my_params.dim());

    MeshTools::Generation::build_square(*mesh_coarse, my_params.n_elem_x(),
                                        my_params.n_elem_y(), my_params.x_min(),
                                        my_params.x_max(), my_params.y_min(),
                                        my_params.y_max(), QUAD4);
    // MeshTools::Generation::build_cube(
    //     *mesh_coarse, my_params.n_elem_x(), my_params.n_elem_y(),
    //     my_params.n_elem_z(), my_params.x_min(), my_params.x_max(),
    //     my_params.y_min(), my_params.y_max(), my_params.z_min(),
    //     my_params.z_max(), HEX8);

    auto ml_hierarchy =
        ML_hierarchy<utopia::PetscMatrix, utopia::PetscVector,
                     // LinearElasticity3D<utopia::PetscMatrix,
                     // utopia::PetscVector>,
                     LinearElasticity<utopia::PetscMatrix, utopia::PetscVector>,
                     libMesh::Mesh>(my_params.n_levels(), my_params.n_vars());

    /////////////////////////////////////////////////////////////////////////////////
    // TODO:: add check for type of mesh - so we do not have to comment in and
    // out stuff
    // auto mesh_coarse =
    // std::make_shared<libMesh::DistributedMesh>(init.comm());
    // mesh_coarse->read(my_params.mesh_path());

    // auto ml_hierarchy =
    //     ML_hierarchy<utopia::PetscMatrix, utopia::PetscVector,
    //                  Poisson<utopia::PetscMatrix, utopia::PetscVector>,
    //                  libMesh::DistributedMesh>(my_params.n_levels(),
    //                                            my_params.n_vars());
    /////////////////////////////////////////////////////////////////////////////////

    ml_hierarchy.generate_ml_hierarchy(*mesh_coarse, my_params);
    utopia::PetscVector solution =
        ml_hierarchy.level_functions_.back()->initial_guess();
    // ml_hierarchy.write_to_exodus(solution, "Hardik_test");
    // exit(0);

    std::cout << "Problem size: " << solution.size() << "   \n";
    auto nlsolver = libmopia::get_nonlinear_solver(my_params, ml_hierarchy);
    nlsolver->solve(*ml_hierarchy.level_functions_.back(), solution);
    auto sol_status = nlsolver->solution_status();
    libmopia::save_statistics(my_params, sol_status, ml_hierarchy);

    if (OptProblemInterface<utopia::PetscMatrix, utopia::PetscVector>
            *opt_problem =
                dynamic_cast<OptProblemInterface<utopia::PetscMatrix,
                                                 utopia::PetscVector> *>(
                    ml_hierarchy.level_functions_.back().get())) {
      opt_problem->write_to_exodus(solution, "my_test_MG.e");
    }
  }

  utopia::Utopia::Finalize();

  return 0;
}