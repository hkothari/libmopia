#ifndef LIBMOPIA_ML_INTERFACE_CPP
#define LIBMOPIA_ML_INTERFACE_CPP

#include "../includes/OptProblemInterface.hpp"

namespace utopia {

using namespace libMesh;

// petsc wrapper for objective function
PetscErrorCode FormObjectiveFunctionTransient(SNES snes, Vec x,
                                              PetscReal *energy, void *ctx) {
  utopia::OptProblemInterface<utopia::PetscMatrix, utopia::PetscVector>
      *ptr_object =
          static_cast<utopia::OptProblemInterface<utopia::PetscMatrix,
                                                  utopia::PetscVector> *>(ctx);

  ptr_object->my_energy(x, energy);

  return 0;
}

template <class Matrix, class Vector>
bool OptProblemInterface<Matrix, Vector>::value(
    const Vector &x, typename Vector::Scalar &result) const {
  DM dm;
  DMSNES sdm;

  SNESGetDM(snes_, &dm);
  DMGetDMSNES(dm, &sdm);
  if (sdm->ops->computeobjective) {
    SNESComputeObjective(snes_, raw_type(x), &result);

  } else {
    Vector grad = 0.0 * x;
    this->gradient(x, grad);
    const Scalar gnorm = norm2(grad);
    result = 0.5 * gnorm * gnorm;
  }

  return true;
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::write_to_exodus(
    const Vector &solution, const std::string file_name) {
  this->assign_solution(solution);
  MeshBase &mesh = es_.get_mesh();
  ExodusII_IO(mesh).write_equation_systems(file_name, es_);
}

template <class Matrix, class Vector>
bool OptProblemInterface<Matrix, Vector>::gradient(const Vector &x,
                                                   Vector &g) const {
  // initialization of gradient vector...
  if (empty(g)) {
    g.zeros(layout(x));
  }

  SNESComputeFunction(snes_, raw_type(x), raw_type(g));

  // TODO:: it should not be here... but libmesh sucks a doing stuff correctly,
  // so why not..
  this->zero_contribution_to_equality_constrains(g);

  // work around to add counter
  OptProblemInterface<Matrix, Vector> *ptr =
      const_cast<OptProblemInterface<Matrix, Vector> *>(this);
  ptr->grad_eval_counter_increase();

  return true;
}

template <class Matrix, class Vector>
bool OptProblemInterface<Matrix, Vector>::hessian(const Vector &x,
                                                  Matrix &hessian) const {
  SNESComputeJacobian(snes_, raw_type(x), snes_->jacobian, snes_->jacobian_pre);
  wrap(snes_->jacobian, hessian);
  return true;
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::init() {
  this->add_bc();

  system_.nonlinear_solver->residual_object = this;
  system_.nonlinear_solver->jacobian_object = this;

  // missing init from other file
  es_.init();

  init_snes();
  init_bc_and_ig(system_);
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::assign_solution(const Vector &x) {
  const MeshBase &mesh = es_.get_mesh();
  libMesh::PetscVector<libMesh::Number> petsc_vec_libmesh_x(utopia::raw_type(x),
                                                            mesh.comm());
  libMesh::NumericVector<libMesh::Number> *numeric_x =
      cast_ptr<libMesh::NumericVector<libMesh::Number> *>(&petsc_vec_libmesh_x);

  *system_.solution = *numeric_x;
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::my_energy(Vec x, PetscReal *energy) {
  EquationSystems &es = system_.get_equation_systems();

  // Get a constant reference to the mesh object.
  const MeshBase &mesh = es.get_mesh();

  libMesh::PetscVector<libMesh::Number> petsc_vec_libmesh_x(x, mesh.comm());

  libMesh::NumericVector<libMesh::Number> *numeric_x =
      cast_ptr<libMesh::NumericVector<libMesh::Number> *>(&petsc_vec_libmesh_x);

  // should be verified once more with proper example
  *system_.solution = *numeric_x;
  system_.solution->close();
  system_.update();

  this->libmesh_energy(*system_.current_local_solution, *energy, system_);

  // we need to reduce to get correct value in parallel
  utopia::PetscCommunicator comm;
  *energy = comm.sum(*energy);
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::init_bc_and_ig(
    libMesh::NonlinearImplicitSystem &sys) {
  using namespace libMesh;

  EquationSystems &es = sys.get_equation_systems();

  std::unique_ptr<NumericVector<libMesh::Number>> sol =
      es.get_system(0).solution->clone();

  libMesh::PetscVector<libMesh::Number> *vector_petsc =
      cast_ptr<libMesh::PetscVector<libMesh::Number> *>(sol.get());

  Vec petsc_vec = vector_petsc->vec();

  utopia::convert(petsc_vec, this->_x_eq_values);
  this->_eq_constrains_flg = 0.0 * this->_x_eq_values;

  // ImplicitSystem &system = es.get_system<ImplicitSystem>(0);
  DofMap &dof_map = sys.get_dof_map();
  const bool has_constaints =
      dof_map.constraint_rows_begin() != dof_map.constraint_rows_end();

  if (has_constaints) {
    libMesh::DofConstraintValueMap &rhs_values =
        dof_map.get_primal_constraint_values();

    {
      utopia::Write<Vector> w_v(this->_x_eq_values);
      utopia::Write<Vector> w_v2(this->_eq_constrains_flg);

      utopia::Range r = range(this->_x_eq_values);
      for (SizeType i = r.begin(); i < r.end(); ++i) {
        if (dof_map.is_constrained_dof(i)) {
          auto valpos = rhs_values.find(i);
          this->_eq_constrains_flg.set(i, 1.0);
          this->_x_eq_values.set(
              i, (valpos == rhs_values.end()) ? 0.0 : valpos->second);
        }
      }
    }
  }

  this->init_constraint_indices();
}

template <class Matrix, class Vector>
void OptProblemInterface<Matrix, Vector>::init_snes() {
  using namespace libMesh;
  auto nl_system =
      dynamic_cast<libMesh::NonlinearImplicitSystem *>(&es_.get_system(0));

  libMesh::PetscNonlinearSolver<libMesh::Number> *petsc_solver =
      dynamic_cast<libMesh::PetscNonlinearSolver<libMesh::Number> *>(
          nl_system->nonlinear_solver.get());
  petsc_solver->init();

  libMesh::PetscMatrix<libMesh::Number> &Jac_sys =
      *libMesh::libmesh_cast_ptr<libMesh::PetscMatrix<libMesh::Number> *>(
          nl_system->matrix);
  libMesh::PetscVector<libMesh::Number> &res_sys =
      *libMesh::libmesh_cast_ptr<libMesh::PetscVector<libMesh::Number> *>(
          nl_system->rhs);

  snes_ = petsc_solver->snes();
  SNESSetFunction(snes_, res_sys.vec(), __libmesh_petsc_snes_residual,
                  petsc_solver);
  SNESSetJacobian(snes_, Jac_sys.mat(), Jac_sys.mat(),
                  __libmesh_petsc_snes_jacobian, petsc_solver);

  SNESSetObjective(snes_, FormObjectiveFunctionTransient, this);

  SNESSetFromOptions(snes_);
}

}; // namespace utopia

#endif