#include "libmesh/equation_systems.h"
#include "libmesh/nonlinear_implicit_system.h"
#include "libmesh/petsc_nonlinear_solver.h"
#include <libmesh/elem.h>
#include <libmesh/exodusII_io.h>
#include <libmesh/libmesh.h>
#include <libmesh/mesh.h>
#include <libmesh/mesh_base.h>
#include <libmesh/mesh_generation.h>
#include <libmesh/mesh_refinement.h>
#include <libmesh/petsc_matrix.h>
#include <libmesh/petsc_vector.h>

#include "utopia_ApproxL2LocalAssembler.hpp"
#include "utopia_InterpolationLocalAssembler.hpp"
#include "utopia_L2LocalAssembler.hpp"
#include "utopia_Local2Global.hpp"
#include "utopia_LocalAssembler.hpp"
#include "utopia_TransferAssembler.hpp"
#include <chrono>
#include <fstream>
#include <utopia.hpp>
#include <utopia_fe.hpp>

#include "includes/Bratu.hpp"
#include "includes/DeSaintVenant.hpp"
#include "includes/LibMopiaInputParams.hpp"
#include "includes/LinearElasticity.hpp"
#include "includes/LinearElasticityTest.hpp"
#include "includes/ML_hierarchy.hpp"
#include "includes/Minsurface.hpp"
#include "includes/Neohookean.hpp"
#include "includes/Poisson.hpp"

#include "src/OptProblemInterface.cpp"

// TODO list
// redo structure of classes
// fix how BC and handled in Opt.Problem interface class
// remove bc conditions from the transfer
// refactor other classes

int main(int argc, char **argv) {
  utopia::Utopia::Init(argc, argv);
  utopia::InputParameters params;
  params.init(argc, argv);

  // check if input file exist
  std::string input;

  if (argc >= 2) {
    input = argv[1];
  } else {
    input = "../inputs/InputPoisson.in";
  }

  GetPot infile(input);
  LibMopiaInputParameters my_params;
  my_params.readParameters(infile);

  {
    using namespace libMesh;
    using namespace utopia;

    // Initialize libMesh and any dependent libraries
    LibMeshInit init(argc, argv, PETSC_COMM_WORLD);

    // generated mesh
    auto mesh = std::make_shared<libMesh::Mesh>(init.comm(), my_params.dim());
    MeshTools::Generation::build_square(
        *mesh, my_params.n_elem_x(), my_params.n_elem_y(), my_params.x_min(),
        my_params.x_max(), my_params.y_min(), my_params.y_max(), QUAD4);

    // Neohookean<utopia::PetscMatrix, utopia::PetscVector> fun(*mesh);
    LinearElasticityTest<utopia::PetscMatrix, utopia::PetscVector> fun(*mesh);

    utopia::PetscVector solution = fun.initial_guess();

    auto linear_solver =
        std::make_shared<ConjugateGradient<utopia::PetscMatrix,
                                           utopia::PetscVector, HOMEMADE>>();

    linear_solver->verbose(true);
    linear_solver->max_it(10000);
    linear_solver->atol(1e-11);
    linear_solver->stol(1e-11);

    auto hess_approx = std::make_shared<JFNK<utopia::PetscVector>>(fun);

    QuasiNewton<utopia::PetscMatrix, utopia::PetscVector> nlsolver(
        hess_approx, linear_solver);

    nlsolver.atol(1e-8);
    nlsolver.rtol(1e-8);
    nlsolver.stol(1e-8);
    nlsolver.max_it(10);
    nlsolver.verbose(true);

    nlsolver.solve(fun, solution);

    fun.write_to_exodus(solution, "test_linear_new.e");
  }

  utopia::Utopia::Finalize();

  return 0;
}