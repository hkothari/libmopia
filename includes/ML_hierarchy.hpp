#ifndef LIBMOPIA_ML_HIERARCHY_HPP
#define LIBMOPIA_ML_HIERARCHY_HPP

#include "../src/OptProblemInterface.cpp"
#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/fe.h"
#include "libmesh/getpot.h"
#include "libmesh/gmv_io.h"
#include "libmesh/libmesh.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/linear_solver.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/node_range.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/optimization_solver.h"
#include "libmesh/optimization_system.h"
#include "libmesh/parallel.h"
#include "libmesh/parallel_mesh.h"
#include "libmesh/petsc_linear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_vector.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/solver_configuration.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/string_to_enum.h"
#include "libmesh/transient_system.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"
#include "utopia_ApproxL2LocalAssembler.hpp"
#include "utopia_InterpolationLocalAssembler.hpp"
#include "utopia_L2LocalAssembler.hpp"
#include "utopia_Local2Global.hpp"
#include "utopia_LocalAssembler.hpp"
#include "utopia_TransferAssembler.hpp"
#include "utopia_assemble_volume_transfer.hpp"
#include <libmesh/explicit_system.h>
#include <libmesh/implicit_system.h>

#include "MassMatrix.hpp"

// Bring in everything from the libMesh namespace
using namespace libMesh;

template <class Matrix, class Vector, class ProblemType, class MeshType>
class ML_hierarchy {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

public:
  ML_hierarchy(const SizeType &n_levels, const SizeType &n_vars)
      : n_levels_(n_levels), n_vars_(n_vars), eps_(1e-8) {}

  void generate_mesh_hierarchy_uniform(MeshType &mesh_coarse) {
    meshes_.resize(n_levels_);

    meshes_[0] = std::make_shared<MeshType>(mesh_coarse);

    for (int l = 1; l < n_levels_; l++) {
      meshes_[l] = std::make_shared<MeshType>(*meshes_[l - 1]);
      MeshRefinement mesh_refinement(*meshes_[l]);
      mesh_refinement.uniformly_refine(1);
    }
  }

  bool assemble_transfer(const LibMopiaInputParameters &params) {
    // assemble 0->1
    auto assembler = std::make_shared<utopia::L2LocalAssembler>(
        meshes_[0]->mesh_dimension(), true, false);

    auto local2global = std::make_shared<utopia::Local2Global>(false);
    utopia::TransferAssembler transfer_assembler(assembler, local2global);

    transfers_.resize(n_levels_ - 1);

    for (int l = 0; l < n_levels_ - 1; l++) {
      EquationSystems equation_systems_coarse(*meshes_[l]);
      EquationSystems equation_systems_finer(*meshes_[l + 1]);

      auto assemble_coarse =
          std::make_shared<MassMatrix<Matrix, Vector>>(equation_systems_coarse);

      auto assemble_finer =
          std::make_shared<MassMatrix<Matrix, Vector>>(equation_systems_finer);

      Matrix M_coarse;
      assemble_coarse->get_mass_matrix(M_coarse);

      Matrix M_fine;
      assemble_finer->get_mass_matrix(M_fine);

      std::vector<std::shared_ptr<Matrix>> mats;
      if (!transfer_assembler.assemble(
              utopia::make_ref(*meshes_[l]),
              utopia::make_ref(assemble_coarse->get_dof_map()),
              utopia::make_ref(*meshes_[l + 1]),
              utopia::make_ref(assemble_finer->get_dof_map()), mats)) {
        return false;
      }

      Vector d = sum(*mats[0], 1);
      Matrix prolongation = utopia::diag(1. / d) * (*mats[0]);

      MatChop(utopia::raw_type(prolongation), eps_);

      // // proper l2-projection - from fine to coarse
      // std::vector<std::shared_ptr<Matrix>> mats_projection;
      // if (!transfer_assembler.assemble(
      //         utopia::make_ref(*meshes_[l + 1]),
      //         utopia::make_ref(assemble_finer->get_dof_map()),
      //         utopia::make_ref(*meshes_[l]),
      //         utopia::make_ref(assemble_coarse->get_dof_map()),
      //         mats_projection)) {
      //   return false;
      // }

      // d = sum(*mats_projection[0], 1);
      // Matrix projection = utopia::diag(1. / d) * (*mats_projection[0]);
      // MatChop(utopia::raw_type(projection), eps_);

      // just for Hardik to save matrices
      // std::string name = "prolongation_" + std::to_string(l) + ".m";
      // utopia::write(name, prolongation);

      Matrix R = transpose(prolongation);
      Matrix inv_lumped_mass = diag(1. / sum(M_coarse, 1));
      Matrix projection = inv_lumped_mass * R * M_fine;

      if (n_vars_ > 1) {
        Matrix prolong_m, project_m;
        MatDuplicate(raw_type(prolongation), MAT_COPY_VALUES,
                     &raw_type(prolong_m));
        splice_interpolation(raw_type(prolongation), &raw_type(prolong_m),
                             n_vars_);

        // std::cout << "n_vars_: " << n_vars_ << " \n";

        MatDuplicate(raw_type(projection), MAT_COPY_VALUES,
                     &raw_type(project_m));
        splice_interpolation(raw_type(projection), &raw_type(project_m),
                             n_vars_);

        // std::cout << "prolong_m. " << prolong_m.size() << "  \n";
        // std::cout << "prolongation. " << prolongation.size() << "  \n";

        if (params.use_restriction()) {

          Scalar scaling_factor = (params.dim() == 2) ? 1. / 4. : 1. / 8.;
          project_m = scaling_factor * transpose(prolong_m);
          std::cout << "--------------------------- using restriction "
                       "--------------------------- \n";
          // project_m = transpose(prolong_m);
        }

        transfers_[l] = std::make_shared<utopia::IPRTransfer<Matrix, Vector>>(
            std::make_shared<Matrix>(prolong_m),
            std::make_shared<Matrix>(project_m));

      } else {

        if (params.use_restriction()) {
          //   std::cout << "--------------------------- using restriction "
          //                "--------------------------- \n";

          Scalar scaling_factor = (params.dim() == 2) ? 1. / 4. : 1. / 8.;
          projection = scaling_factor * R;
        }

        transfers_[l] = std::make_shared<utopia::IPRTransfer<Matrix, Vector>>(
            std::make_shared<Matrix>(prolongation),
            std::make_shared<Matrix>(projection));
      }
    }

    return true;
  }

  void generate_level_functions(const LibMopiaInputParameters &params) {
    level_functions_.resize(n_levels_);

    for (SizeType i = 0; i < n_levels_; i++) {
      // auto nodes = meshes_[i]->n_nodes();

      auto fun = std::make_shared<ProblemType>(*meshes_[i]);
      fun->read_params(params);
      level_functions_[i] = fun;
    }
  }

  void generate_ml_hierarchy(MeshType &mesh_coarse,
                             const LibMopiaInputParameters &params) {
    generate_mesh_hierarchy_uniform(mesh_coarse);
    assemble_transfer(params);
    generate_level_functions(params);
  }

  SizeType n_levels() const { return n_levels_; }

  void write_to_exodus(const Vector &solution,
                       const std::string file_name = "output") {
    Vector sol = solution;

    if (utopia::OptProblemInterface<Matrix, Vector> *opt_problem =
            dynamic_cast<utopia::OptProblemInterface<Matrix, Vector> *>(
                level_functions_.back().get())) {
      std::string name = file_name + "_" + std::to_string(n_levels_ - 1) + ".e";
      opt_problem->write_to_exodus(sol, name);
    }

    for (SizeType i = n_levels_ - 2; i >= 0; i--) {
      transfers_[i]->project_down(sol, sol);

      std::string name = file_name + "_" + std::to_string(i) + ".e";

      if (utopia::OptProblemInterface<Matrix, Vector> *opt_problem =
              dynamic_cast<utopia::OptProblemInterface<Matrix, Vector> *>(
                  level_functions_[i].get())) {
        opt_problem->write_to_exodus(sol, name);
      }
    }
  }

  Scalar eval_comp_cost() const {

    std::cout << "----- Computational cost ------ \n";
    SizeType dim = meshes_[0]->mesh_dimension();
    Scalar comp_cost_grad = 0.0;

    for (int l = 0; l < n_levels_; l++) {
      if (utopia::OptProblemInterface<Matrix, Vector> *opt_problem =
              dynamic_cast<utopia::OptProblemInterface<Matrix, Vector> *>(
                  level_functions_[l].get())) {
        Scalar num_grads_level = opt_problem->grad_evals();
        Scalar scaling_factor = dim * (n_levels_ - l - 1);
        scaling_factor = std::pow(2, -scaling_factor);
        comp_cost_grad += scaling_factor * num_grads_level;
        std::cout << "l: " << l << "  #g: " << num_grads_level << " \n";
      }
    }

    std::cout << "total_grad_eval: " << comp_cost_grad << " \n";
    return comp_cost_grad;
  }

private:
  void splice_interpolation(Mat In, Mat *Out, int dim) {
    PetscInt rank, size;
    Mat TMP;

    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
    MPI_Comm_size(PETSC_COMM_WORLD, &size);

    MatCreateMAIJ(In, dim, &TMP);

    if (size > 1) {
      MatConvert(TMP, MATMPIAIJ, MAT_INITIAL_MATRIX, Out);
    } else {
      MatConvert(TMP, MATAIJ, MAT_INITIAL_MATRIX, Out);
    }

    MatDestroy(&TMP);
  };

public:
  SizeType n_levels_;
  SizeType n_vars_;
  Scalar eps_;
  std::vector<std::shared_ptr<MeshType>> meshes_;
  std::vector<EquationSystems> eq_systems_;

public:
  std::vector<std::shared_ptr<utopia::Transfer<Matrix, Vector>>> transfers_;

  std::vector<std::shared_ptr<utopia::ExtendedFunction<Matrix, Vector>>>
      level_functions_;
};

#endif