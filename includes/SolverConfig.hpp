#ifndef SOLVER_CONFIG_HPP
#define SOLVER_CONFIG_HPP

#include "LibMopiaInputParams.hpp"
#include "ML_hierarchy.hpp"
#include <utopia.hpp>

namespace libmopia {

// Bring in everything from the utopia namespace
using namespace utopia;

template <class Matrix, class Vector, class ProblemType, class MeshType>
std::shared_ptr<QuasiNewton<Matrix, Vector>> get_JFNK_CG_MG(

    const LibMopiaInputParameters &my_params,
    const ML_hierarchy<Matrix, Vector, ProblemType, MeshType> &ml_hierarchy) {

  auto jfnk_mg =
      std::make_shared<JFNK_Multigrid<Matrix, Vector>>(ml_hierarchy.n_levels());

  auto smoother = std::make_shared<Chebyshev3level<Matrix, Vector, HOMEMADE>>();
  smoother->atol(1e-15);
  smoother->stol(1e-15);
  smoother->rtol(1e-15);
  smoother->power_method_max_it(30);
  smoother->power_method_tol(1e-2);
  smoother->norm_frequency(0);
  smoother->verbose(false);

  // auto coarse_grid_solver =
  //     std::make_shared<utopia::ConjugateGradient<Matrix, Vector,
  //     HOMEMADE>>();

  auto lbfgs_precond = std::make_shared<LBFGSInv<Vector>>(20);
  auto coarse_grid_solver = std::make_shared<
      utopia::ConjugateGradientQNPrecond<Matrix, Vector, HOMEMADE>>(
      lbfgs_precond);
  coarse_grid_solver->sample_only_once(true);
  coarse_grid_solver->reset_precond_update(true);

  coarse_grid_solver->atol(1e-12);
  coarse_grid_solver->stol(1e-12);
  coarse_grid_solver->rtol(1e-12);

  if (my_params.dim() == 3) {
    coarse_grid_solver->max_it((my_params.n_elem_x() + 1) *
                               (my_params.n_elem_y() + 1) *
                               (my_params.n_elem_z() + 1) * my_params.n_vars());
  }

  if (my_params.dim() == 2) {
    coarse_grid_solver->max_it((my_params.n_elem_x() + 1) *
                               (my_params.n_elem_y() + 1) * my_params.n_vars());
  }

  coarse_grid_solver->verbose(false);

  jfnk_mg->set_coarse_grid_solver(coarse_grid_solver);
  jfnk_mg->set_smoother(smoother);
  jfnk_mg->norm_frequency(0);
  jfnk_mg->mg_type(1);
  jfnk_mg->pre_smoothing_steps(5);
  jfnk_mg->post_smoothing_steps(5);

  jfnk_mg->set_transfer_operators(ml_hierarchy.transfers_);
  jfnk_mg->set_functions(ml_hierarchy.level_functions_);
  jfnk_mg->max_it(1);
  jfnk_mg->norm_frequency(0);
  jfnk_mg->atol(1e-12);
  jfnk_mg->rtol(1e-12);
  jfnk_mg->stol(1e-12);
  jfnk_mg->verbose(false);

  auto hess_approx =
      std::make_shared<JFNK<Vector>>(*ml_hierarchy.level_functions_.back());

  // auto linear_solver =
  //     std::make_shared<ConjugateGradient<Matrix, Vector, HOMEMADE>>();
  auto linear_solver =
      std::make_shared<SteihaugToint<Matrix, Vector, HOMEMADE>>();

  linear_solver->verbose(false);
  linear_solver->atol(1e-8);
  linear_solver->stol(1e-8);
  linear_solver->rtol(1e-8);
  linear_solver->set_preconditioner(jfnk_mg);

  auto nlsolver =
      std::make_shared<QuasiNewton<Matrix, Vector>>(hess_approx, linear_solver);
  nlsolver->atol(1e-6);
  nlsolver->rtol(1e-12);
  nlsolver->stol(1e-12);
  nlsolver->max_it(30);
  nlsolver->verbose(true);

  // auto ls_strat = std::make_shared<utopia::SimpleBacktracking<Vector>>();
  auto ls_strat = std::make_shared<utopia::Backtracking<Vector>>();
  nlsolver->set_line_search_strategy(ls_strat);

  nlsolver->forcing_strategy(
      utopia::InexactNewtonForcingStartegies::QUADRATIC_2);

  return nlsolver;
}

template <class Matrix, class Vector, class ProblemType, class MeshType>
std::shared_ptr<QuasiNewton<Matrix, Vector>> get_JFNK_CG(

    const LibMopiaInputParameters &my_params,
    const ML_hierarchy<Matrix, Vector, ProblemType, MeshType> &ml_hierarchy) {

  auto hess_approx =
      std::make_shared<JFNK<Vector>>(*ml_hierarchy.level_functions_.back());

  // auto linear_solver =
  //     std::make_shared<ConjugateGradient<Matrix, Vector, HOMEMADE>>();
  auto linear_solver =
      std::make_shared<SteihaugToint<Matrix, Vector, HOMEMADE>>();

  linear_solver->verbose(false);
  linear_solver->atol(1e-8);
  linear_solver->stol(1e-8);
  linear_solver->rtol(1e-8);
  linear_solver->max_it(1e6);

  auto nlsolver =
      std::make_shared<QuasiNewton<Matrix, Vector>>(hess_approx, linear_solver);
  nlsolver->atol(1e-6);
  nlsolver->rtol(1e-12);
  nlsolver->stol(1e-12);
  nlsolver->max_it(30);
  nlsolver->verbose(true);

  // auto ls_strat = std::make_shared<utopia::SimpleBacktracking<Vector>>();
  auto ls_strat = std::make_shared<utopia::Backtracking<Vector>>();
  nlsolver->set_line_search_strategy(ls_strat);

  nlsolver->forcing_strategy(
      utopia::InexactNewtonForcingStartegies::QUADRATIC_2);

  return nlsolver;
}

template <class Matrix, class Vector, class ProblemType, class MeshType>
std::shared_ptr<QuasiNewton<Matrix, Vector>> get_JFNK_CG_LBFGS(

    const LibMopiaInputParameters &my_params,
    const ML_hierarchy<Matrix, Vector, ProblemType, MeshType> &ml_hierarchy) {

  auto hess_approx =
      std::make_shared<JFNK<Vector>>(*ml_hierarchy.level_functions_.back());

  auto lbfgs_precond = std::make_shared<LBFGSInv<Vector>>(20);
  auto linear_solver = std::make_shared<
      utopia::ConjugateGradientQNPrecond<Matrix, Vector, HOMEMADE>>(
      lbfgs_precond);
  linear_solver->sample_only_once(true);
  linear_solver->reset_precond_update(false);
  linear_solver->verbose(false);
  linear_solver->atol(1e-8);
  linear_solver->stol(1e-8);
  linear_solver->rtol(1e-8);
  linear_solver->max_it(1e6);

  auto nlsolver =
      std::make_shared<QuasiNewton<Matrix, Vector>>(hess_approx, linear_solver);
  nlsolver->atol(1e-6);
  nlsolver->rtol(1e-12);
  nlsolver->stol(1e-12);
  nlsolver->max_it(10);
  nlsolver->verbose(true);

  // auto ls_strat = std::make_shared<utopia::SimpleBacktracking<Vector>>();
  auto ls_strat = std::make_shared<utopia::Backtracking<Vector>>();
  nlsolver->set_line_search_strategy(ls_strat);

  // nlsolver->forcing_strategy(
  //     utopia::InexactNewtonForcingStartegies::QUADRATIC_2);

  return nlsolver;
}

template <class Matrix, class Vector, class ProblemType, class MeshType>
std::shared_ptr<QuasiNewton<Matrix, Vector>> get_nonlinear_solver(
    const LibMopiaInputParameters &my_params,
    const ML_hierarchy<Matrix, Vector, ProblemType, MeshType> &ml_hierarchy) {

  std::shared_ptr<utopia::QuasiNewton<utopia::PetscMatrix, utopia::PetscVector>>
      nlsolver;

  if (my_params.ls_solver_type() == "CG-MG") {
    std::cout << "-------------------- CG-MG -------------------- \n";
    nlsolver = libmopia::get_JFNK_CG_MG(my_params, ml_hierarchy);
  } else if (my_params.ls_solver_type() == "CG") {
    std::cout << "-------------------- CG -------------------- \n";
    nlsolver = libmopia::get_JFNK_CG(my_params, ml_hierarchy);
  } else if (my_params.ls_solver_type() == "CG-LBFGS") {
    std::cout << "-------------------- CG-LBFGS -------------------- \n";
    nlsolver = libmopia::get_JFNK_CG_LBFGS(my_params, ml_hierarchy);
  } else {
    utopia_warning("-------- wrong choice of linear solver, returning single "
                   "level CG ---- \n");
    nlsolver = libmopia::get_JFNK_CG(my_params, ml_hierarchy);
  }

  return nlsolver;
}

template <class Matrix, class Vector, class ProblemType, class MeshType>
void save_statistics(
    const LibMopiaInputParameters &my_params,
    const utopia::SolutionStatus &sol_status,
    const ML_hierarchy<Matrix, Vector, ProblemType, MeshType> &ml_hierarchy) {

  using Scalar = typename Traits<Vector>::Scalar;

  std::string path =
      my_params.ls_solver_type() + "_" + my_params.log_output_path();
  auto comp_cost = ml_hierarchy.eval_comp_cost();

  if (!path.empty()) {
    CSVWriter writer{};
    if (mpi_world_rank() == 0) {
      if (!writer.file_exists(path)) {
        writer.open_file(path);
        writer.write_table_row<std::string>({"n_levels", "nonl_its",
                                             "sum_lin_its", "av_lin_its",
                                             "grad_evals"});
      } else
        writer.open_file(path);

      writer.write_table_row<Scalar>(
          {Scalar(ml_hierarchy.n_levels()), Scalar(sol_status.iterates),
           Scalar(sol_status.sum_linear_its),
           Scalar(sol_status.sum_linear_its) / Scalar(sol_status.iterates),
           comp_cost});
      writer.close_file();
    }
  }
}

}; // namespace libmopia

#endif // SOLVER_CONFIG_HPP