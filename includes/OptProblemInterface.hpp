#ifndef PETSC_BASED_UTOPIA_NONLINEAR_FUNCTION_NEW_HPP
#define PETSC_BASED_UTOPIA_NONLINEAR_FUNCTION_NEW_HPP

#include "LibMopiaInputParams.hpp"
#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/fe.h"
#include "libmesh/getpot.h"
#include "libmesh/gmv_io.h"
#include "libmesh/libmesh.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/linear_solver.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/node_range.h"
#include "libmesh/nonlinear_implicit_system.h"
#include "libmesh/nonlinear_solver.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/optimization_solver.h"
#include "libmesh/optimization_system.h"
#include "libmesh/parallel.h"
#include "libmesh/parallel_mesh.h"
#include "libmesh/petsc_linear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_nonlinear_solver.h"
#include "libmesh/petsc_vector.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/solver_configuration.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/string_to_enum.h"
#include "libmesh/transient_system.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"
#include <libmesh/elem.h>
#include <libmesh/exodusII_io.h>
#include <libmesh/libmesh.h>
#include <libmesh/mesh.h>
#include <libmesh/mesh_base.h>
#include <libmesh/mesh_generation.h>
#include <libmesh/mesh_refinement.h>
#include <libmesh/petsc_matrix.h>
#include <libmesh/petsc_vector.h>
#include <utopia.hpp>

#include <petsc/private/snesimpl.h>
#include <petscsnes.h>

namespace utopia {

using namespace libMesh;

template <class Matrix, class Vector>
class OptProblemInterface : public utopia::ExtendedFunction<Matrix, Vector>,
                            public NonlinearImplicitSystem::ComputeResidual,
                            public NonlinearImplicitSystem::ComputeJacobian {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

public:
  OptProblemInterface(libMesh::MeshBase &mesh, std::string name = "MyOptSystem")
      : ExtendedFunction<Matrix, Vector>(
            Vector(layout(utopia::PetscCommunicator::world(), 1, 1), 0.0),
            Vector(layout(utopia::PetscCommunicator::world(), 1, 1), 0.0)),
        es_(libMesh::EquationSystems(mesh)),
        system_(es_.add_system<libMesh::NonlinearImplicitSystem>(name)) {}

  bool value(const Vector &x, typename Vector::Scalar &result) const override;

  bool gradient(const Vector &x, Vector &g) const override;

  bool hessian(const Vector &x, Matrix &hessian) const override;

  void write_to_exodus(const Vector &solution,
                       const std::string file_name = "output.e");

  virtual void read_params(const LibMopiaInputParameters & /*params*/) {}

public:
  virtual void my_energy(Vec x, PetscReal *energy);

protected:
  virtual void jacobian(const libMesh::NumericVector<libMesh::Number> &soln,
                        libMesh::SparseMatrix<libMesh::Number> &jacobian,
                        libMesh::NonlinearImplicitSystem &sys) override = 0;

  virtual void residual(const libMesh::NumericVector<libMesh::Number> &soln,
                        libMesh::NumericVector<libMesh::Number> &residual,
                        libMesh::NonlinearImplicitSystem &sys) override = 0;

  virtual void
  libmesh_energy(const libMesh::NumericVector<libMesh::Number> &soln,
                 libMesh::Number &energy,
                 libMesh::NonlinearImplicitSystem &sys) = 0;

  virtual void add_bc() = 0;

  void init();

public:
  SizeType grad_evals() const { return grad_eval_counter_; }
  void grad_eval_counter_increase() { grad_eval_counter_++; }
  void reset_grad_eval_counter() { grad_eval_counter_ = 0.0; }

private:
  void init_snes();

  void init_bc_and_ig(libMesh::NonlinearImplicitSystem &sys);

  void assign_solution(const Vector &x);

protected:
  libMesh::EquationSystems es_;
  libMesh::NonlinearImplicitSystem &system_;
  SNES snes_;

  SizeType grad_eval_counter_{0};
};

} // namespace utopia

#endif // PETSC_BASED_UTOPIA_NONLINEAR_FUNCTION_NEW_HPP
