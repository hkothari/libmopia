#ifndef LIBMOPIA_NEOHOOOK_HPP
#define LIBMOPIA_NEOHOOOK_HPP
#define _USE_MATH_DEFINES

#include <cmath>
#include <utopia.hpp>

#include "LinearElasticity.hpp"
#include "LinearElasticity3D.hpp"
#include "OptProblemInterface.hpp"
#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/fe.h"
#include "libmesh/node_range.h"
#include "libmesh/parallel.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"

// Bring in everything from the libMesh namespace
using namespace libMesh;

class NeoDispBC : public FunctionBase<Number> {
 public:
  NeoDispBC(unsigned int u_var, unsigned int v_var, unsigned int w_var)
      : _u_var(u_var), _v_var(v_var), _w_var(w_var) {
    this->_initialized = true;
  }

  virtual Number operator()(const Point &, const Real = 0) {
    libmesh_not_implemented();
  }

  virtual void operator()(const Point &p, const Real,
                          DenseVector<Number> &output) {
    output.resize(3);
    output.zero();

    const Real x = p(0);
    const Real y = p(1);
    const Real z = p(2);

    const Real theta = M_PI / 4.;  // 1.04719755;
    const Real scale = 0.9;

    // Center of rotation
    const Real y0 = 0.5;
    const Real z0 = 0.5;

    const Real temp_y = y0 + (y - y0) * std::cos(theta) - (z - z0) * sin(theta);
    const Real temp_z = z0 + (y - y0) * std::sin(theta) + (z - z0) * cos(theta);

    output(_u_var) = 0.;
    output(_v_var) = scale * (temp_y - y);
    output(_w_var) = scale * (temp_z - z);
  }

  virtual std::unique_ptr<FunctionBase<Number>> clone() const {
    return libmesh_make_unique<NeoDispBC>(_u_var, _v_var, _w_var);
  }

 private:
  const unsigned int _u_var;
  const unsigned int _v_var;
  const unsigned int _w_var;
};

template <class Matrix, class Vector>
class Neohookean final : public utopia::OptProblemInterface<Matrix, Vector> {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

 public:
  Neohookean(MeshBase &mesh, const Number &young_modulus = 10.0,
             const Number &poisson_ratio = 0.3,
             const Number &forcing_magnitude = -1e-5,
             const Number &neumann_magnitude = -1e-5)
      : utopia::OptProblemInterface<Matrix, Vector>(mesh, "Neohookean"),
        u_var_(this->system_.add_variable(
            "dispx", Utility::string_to_enum<Order>("FIRST"))),
        v_var_(this->system_.add_variable(
            "dispy", Utility::string_to_enum<Order>("FIRST"))),
        w_var_(this->system_.add_variable(
            "dispz", Utility::string_to_enum<Order>("FIRST"))),
        _young_modulus(young_modulus),
        _poisson_ratio(poisson_ratio),
        _forcing_magnitude(forcing_magnitude),
        _neumann_magnitude(neumann_magnitude) {
    this->init();

    // std::cout << "rhs: " << _forcing_magnitude << "  \n";
    // this->assemble_rhs();
    // // exit(0);

    _mu = _young_modulus / (2. + (2. * _poisson_ratio));
    _lambda = (_young_modulus * _poisson_ratio) /
              ((1. + _poisson_ratio) * (1. - (2. * _poisson_ratio)));

    f_vec_ = VectorValue<Number>(0.0, _forcing_magnitude, 0.0);
    g_vec_ = VectorValue<Number>(0.0, 0.0, _neumann_magnitude);
  }

  void read_params(const LibMopiaInputParameters &params) override {
    _young_modulus = params.young_modulus();
    _poisson_ratio = params.poisson_ratio();
    _forcing_magnitude = params.forcing_magnitude();
    _neumann_magnitude = params.neumann_magnitude();

    // std::cout << "1rhs: " << _forcing_magnitude << "  \n";
    // this->assemble_rhs();

    _mu = _young_modulus / (2. + (2. * _poisson_ratio));
    _lambda = (_young_modulus * _poisson_ratio) /
              ((1. + _poisson_ratio) * (1. - (2. * _poisson_ratio)));

    f_vec_ = VectorValue<Number>(0.0, _forcing_magnitude, 0.0);
    g_vec_ = VectorValue<Number>(0.0, 0.0, _neumann_magnitude);

    // add what you need
  }

  void add_bc() override {
    std::set<boundary_id_type> clamped_boundaries;

    clamped_boundaries.insert(BOUNDARY_ID_MAX_X);

    std::vector<unsigned int> variables;
    variables.push_back(u_var_);
    variables.push_back(v_var_);
    variables.push_back(w_var_);

    ZeroFunction<Number> zero;

    this->system_.get_dof_map().add_dirichlet_boundary((DirichletBoundary(
        clamped_boundaries, variables, zero, LOCAL_VARIABLE_ORDER)));

    // //
    // //////////////////////////////////////////////////////////////////////////////////////
    NeoDispBC forceBC(u_var_, v_var_, w_var_);

    std::vector<unsigned int> variables_force;
    variables_force.push_back(u_var_);
    variables_force.push_back(v_var_);
    variables_force.push_back(w_var_);

    std::set<boundary_id_type> force_boundaries;
    force_boundaries.insert(BOUNDARY_ID_MIN_X);

    this->system_.get_dof_map().add_dirichlet_boundary(
        (DirichletBoundary(force_boundaries, variables_force, &forceBC)));
  }

  void residual(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::NumericVector<libMesh::Number> &residual,
                libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.-----------------------------
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running----------------------------------------
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving -----------
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const unsigned int u_var = system.variable_number("dispx");

    // GetDofMAP of the system -------------------------------------------------
    const DofMap &dof_map = system.get_dof_map();

    FEType fe_type = dof_map.variable_type(u_var);

    // Get elem quadrature rules system ----------------------------------------
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    // Get face quadrature rules system ----------------------------------------
    std::unique_ptr<FEBase> fe_face(FEBase::build(dim, fe_type));
    QGauss qface(dim - 1, fe_type.default_quadrature_order());
    fe_face->attach_quadrature_rule(&qface);

    const std::vector<Real> &JxW_face = fe_face->get_JxW();
    const std::vector<std::vector<Real>> &phi_face = fe_face->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi_face =
        fe_face->get_dphi();

    // Get dof indices ---------------------------------------------------------
    std::vector<dof_id_type> dof_indices;
    std::vector<std::vector<dof_id_type>> dof_indices_var(3);

    // declare space for elemental residual ------------------------------------
    residual.zero();

    DenseVector<Number> Re;
    DenseSubVector<Number> Re_var[3] = {DenseSubVector<Number>(Re),
                                        DenseSubVector<Number>(Re),
                                        DenseSubVector<Number>(Re)};

    // Element loop  -----------------------------------------------------------
    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      for (unsigned int var = 0; var < 3; var++) {
        dof_map.dof_indices(elem, dof_indices_var[var], var);
      }

      const unsigned int n_dofs = dof_indices.size();
      const unsigned int n_var_dofs = dof_indices_var[0].size();

      fe->reinit(elem);

      Re.resize(n_dofs);
      for (unsigned int var = 0; var < 3; var++) {
        Re_var[var].reposition(var * n_var_dofs, n_var_dofs);
      }

      // qp loop --------------------------------------------------------------
      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        VectorValue<Number> u_vec;
        TensorValue<Number> grad_u;

        for (unsigned int var_i = 0; var_i < 3; var_i++) {
          for (unsigned int j = 0; j < n_var_dofs; j++) {
            u_vec(var_i) += phi[j][qp] * soln(dof_indices_var[var_i][j]);
          }

          // Row is variable u, v, or w column is x, y, or z
          for (unsigned int var_j = 0; var_j < 3; var_j++) {
            for (unsigned int j = 0; j < n_var_dofs; j++) {
              grad_u(var_i, var_j) +=
                  dphi[j][qp](var_j) * soln(dof_indices_var[var_i][j]);
            }
          }
        }

        // Define the deformation gradient
        TensorValue<Number> F = grad_u;
        for (unsigned int var = 0; var < 3; var++) {
          F(var, var) += 1.;
        }

        Number detF = F.det();
        assert(detF > 0);

        TensorValue<Number> C = F.transpose() * F;
        TensorValue<Number> invC = C.inverse();
        TensorValue<Number> stress_tensor;
        const double log_detF = std::log(detF);

        TensorValue<Number> Id_min_invC = -1. * invC;
        for (unsigned int var = 0; var < 3; var++) {
          Id_min_invC(var, var) += 1.;
        };

        stress_tensor = (_mu * Id_min_invC) + (_lambda * log_detF * invC);

        TensorValue<Number> FxS = F * stress_tensor;

        for (unsigned int dof_i = 0; dof_i < n_var_dofs; dof_i++) {
          for (unsigned int i = 0; i < 3; i++) {
            for (unsigned int j = 0; j < 3; j++) {
              Re_var[i](dof_i) += JxW[qp] * (FxS(i, j) * dphi[dof_i][qp](j));
            }

            Re_var[i](dof_i) -= JxW[qp] * (f_vec_(i) * phi[dof_i][qp]);
          }
        }
      }  // end of qp ------------------------------------------------

      // loop for neumann boundary condition -------------------------
      for (auto side : elem->side_index_range()) {
        if (elem->neighbor_ptr(side) == nullptr) {
          fe_face->reinit(elem, side);

          // Apply a traction
          for (unsigned int qp = 0; qp < qface.n_points(); qp++) {
            if (mesh.get_boundary_info().has_boundary_id(elem, side,
                                                         BOUNDARY_ID_MAX_Z)) {
              for (unsigned int dof_i = 0; dof_i < n_var_dofs; dof_i++) {
                for (unsigned int i = 0; i < dim; i++) {
                  Re_var[i](dof_i) -=
                      JxW_face[qp] * (g_vec_(i) * phi_face[dof_i][qp]);
                }
              }
            }
          }  // end of qp side ---------------------------------------
        }
      }  // end of side loop -----------------------------------------

      dof_map.constrain_element_vector(Re, dof_indices);
      residual.add_vector(Re, dof_indices);
    }  // end of elem ------------------------------------------------
  }    // end compute residual ---------------------------------------

  /**
   * Function which computes the jacobian.
   */
  void jacobian(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::SparseMatrix<libMesh::Number> &jacobian,
                libMesh::NonlinearImplicitSystem &sys) override {
    std::cout << "--------- empty ---------- \n";
  }

  void libmesh_energy(const libMesh::NumericVector<libMesh::Number> &soln,
                      libMesh::Number &energy,
                      libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.-----------------------------
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running----------------------------------------
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving -----------
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const unsigned int u_var = system.variable_number("dispx");

    // GetDofMAP of the system -------------------------------------------------
    const DofMap &dof_map = system.get_dof_map();

    FEType fe_type = dof_map.variable_type(u_var);

    // Get elem quadrature rules system ----------------------------------------
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    // Get face quadrature rules system ----------------------------------------
    std::unique_ptr<FEBase> fe_face(FEBase::build(dim, fe_type));
    QGauss qface(dim - 1, fe_type.default_quadrature_order());
    fe_face->attach_quadrature_rule(&qface);

    const std::vector<Real> &JxW_face = fe_face->get_JxW();
    const std::vector<std::vector<Real>> &phi_face = fe_face->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi_face =
        fe_face->get_dphi();

    // Get dof indices ---------------------------------------------------------
    std::vector<dof_id_type> dof_indices;
    std::vector<std::vector<dof_id_type>> dof_indices_var(3);

    energy = 0.0;

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      for (unsigned int var = 0; var < 3; var++) {
        dof_map.dof_indices(elem, dof_indices_var[var], var);
      }

      const unsigned int n_dofs = dof_indices.size();
      const unsigned int n_var_dofs = dof_indices_var[0].size();

      fe->reinit(elem);

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        VectorValue<Number> u_vec;
        TensorValue<Number> grad_u;

        for (unsigned int var_i = 0; var_i < 3; var_i++) {
          for (unsigned int j = 0; j < n_var_dofs; j++) {
            u_vec(var_i) += phi[j][qp] * soln(dof_indices_var[var_i][j]);
          }

          // Row is variable u, v, or w column is x, y, or z
          for (unsigned int var_j = 0; var_j < 3; var_j++) {
            for (unsigned int j = 0; j < n_var_dofs; j++) {
              grad_u(var_i, var_j) +=
                  dphi[j][qp](var_j) * soln(dof_indices_var[var_i][j]);
            }
          }
        }

        // Define the deformation gradient
        auto F = grad_u;
        for (unsigned int var = 0; var < 3; var++) {
          F(var, var) += 1.;
        }

        Number detF = F.det();

        assert(detF > 0);

        TensorValue<Number> C = F.transpose() * F;

        Number I_C = C.tr();

        const double log_detF = std::log(detF);

        energy += JxW[qp] * (((_mu / 2.0) * (I_C - 3.0)) - (_mu * log_detF) +
                             ((_lambda / 2.0) * log_detF * log_detF));

        for (unsigned int i = 0; i < 3; i++) {
          energy -= JxW[qp] * (f_vec_(i) * u_vec(i));
        }
      }  // end of qp loop

      // loop for neumann boundary condition -------------------------
      for (auto side : elem->side_index_range()) {
        if (elem->neighbor_ptr(side) == nullptr) {
          fe_face->reinit(elem, side);

          // Apply a traction
          for (unsigned int qp = 0; qp < qface.n_points(); qp++) {
            if (mesh.get_boundary_info().has_boundary_id(elem, side,
                                                         BOUNDARY_ID_MAX_Z)) {
              VectorValue<Number> u_vec_side;

              for (unsigned int var_i = 0; var_i < 3; var_i++) {
                for (unsigned int j = 0; j < n_var_dofs; j++) {
                  u_vec_side(var_i) +=
                      phi_face[j][qp] * soln(dof_indices_var[var_i][j]);
                }
              }
              for (unsigned int i = 0; i < dim; i++) {
                energy -= JxW_face[qp] * (g_vec_(i) * u_vec_side(i));
              }
            }
          }  // end of qp side ---------------------------------------
        }
      }  // end of side loop -----------------------------------------
    }
  }

 private:
  Number _young_modulus;
  Number _poisson_ratio;
  Number _forcing_magnitude;
  Number _neumann_magnitude;

  Number _mu;
  Number _lambda;

  unsigned int u_var_;
  unsigned int v_var_;
  unsigned int w_var_;

  std::unique_ptr<NumericVector<Number>> force_;
  VectorValue<Number> f_vec_;
  VectorValue<Number> g_vec_;
};

#endif