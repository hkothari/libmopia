#ifndef LIBMOPIA_BRATU_HPP
#define LIBMOPIA_BRATU_HPP

#include "libmesh/nonlinear_implicit_system.h"
#include "libmesh/nonlinear_solver.h"
#include <libmesh/explicit_system.h>
#include <libmesh/implicit_system.h>

#include <utopia.hpp>

#include "LibMopiaInputParams.hpp"
#include "OptProblemInterface.hpp"
#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/fe.h"
#include "libmesh/getpot.h"
#include "libmesh/gmv_io.h"
#include "libmesh/libmesh.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/linear_solver.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/node_range.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/optimization_solver.h"
#include "libmesh/optimization_system.h"
#include "libmesh/parallel.h"
#include "libmesh/parallel_mesh.h"
#include "libmesh/petsc_linear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_vector.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/solver_configuration.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/string_to_enum.h"
#include "libmesh/transient_system.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"

// Bring in everything from the libMesh namespace
using namespace libMesh;

template <class Matrix, class Vector>
class Bratu final : public utopia::OptProblemInterface<Matrix, Vector> {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

public:
  Bratu(MeshBase &mesh, const Number &lambda = 5.0)
      : utopia::OptProblemInterface<Matrix, Vector>(mesh, "Bratu"),
        u_var_(this->system_.add_variable(
            "u", Utility::string_to_enum<Order>("FIRST"))),
        _lambda(lambda) {
    this->init();
  }

  void read_params(const LibMopiaInputParameters &params) override {
    _lambda = params.lambda();
  }

  void add_bc() override {
    std::set<boundary_id_type> clamped_boundaries;
    clamped_boundaries.insert(0);
    clamped_boundaries.insert(1);
    clamped_boundaries.insert(2);
    clamped_boundaries.insert(3);

    std::vector<unsigned int> variables;
    variables.push_back(u_var_);

    ZeroFunction<> zf;
    DirichletBoundary dirichlet_bc(clamped_boundaries, variables, zf,
                                   LOCAL_VARIABLE_ORDER);

    this->system_.get_dof_map().add_dirichlet_boundary(dirichlet_bc);
  }

  void residual(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::NumericVector<libMesh::Number> &residual,
                libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const DofMap &dof_map = system.get_dof_map();
    FEType fe_type = dof_map.variable_type(0);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    std::vector<dof_id_type> dof_indices;

    DenseVector<Number> Re;

    residual.zero();

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      const unsigned int n_dofs = dof_indices.size();

      fe->reinit(elem);

      Re.resize(n_dofs);

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        Number u = 0;
        Gradient grad_u;
        for (unsigned int j = 0; j < n_dofs; j++) {
          u += phi[j][qp] * soln(dof_indices[j]);
          grad_u += dphi[j][qp] * soln(dof_indices[j]);
        }

        for (unsigned int i = 0; i < n_dofs; i++) {
          Re(i) += JxW[qp] * ((dphi[i][qp] * grad_u) -
                              _lambda * std::exp(u) * phi[i][qp]);
        }
      }

      // This will zero entries of Re corresponding to Dirichlet dofs.
      dof_map.constrain_element_vector(Re, dof_indices);
      residual.add_vector(Re, dof_indices);
    }
  }

  /**
   * Function which computes the jacobian.
   */
  void jacobian(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::SparseMatrix<libMesh::Number> &jacobian,
                libMesh::NonlinearImplicitSystem &sys) override {
    std::cout << "--------- empty atm ---------- \n";
  }

  void libmesh_energy(const libMesh::NumericVector<libMesh::Number> &soln,
                      libMesh::Number &energy,
                      libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const DofMap &dof_map = system.get_dof_map();
    FEType fe_type = dof_map.variable_type(0);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    std::vector<dof_id_type> dof_indices;

    energy = 0.0;

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      const unsigned int n_dofs = dof_indices.size();

      fe->reinit(elem);

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        Number u = 0;
        Gradient grad_u;
        for (unsigned int j = 0; j < n_dofs; j++) {
          u += phi[j][qp] * soln(dof_indices[j]);
          grad_u += dphi[j][qp] * soln(dof_indices[j]);
        }
        energy +=
            JxW[qp] * (0.5 * std::abs(grad_u * grad_u) - _lambda * std::exp(u));
      }
    }
  }

private:
  unsigned int u_var_;
  Number _lambda;
};

#endif // LIBMOPIA_BRATU_HPP