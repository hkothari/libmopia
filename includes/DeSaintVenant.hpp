#ifndef LIBMOPIA_DE_SAINT_VENANT_H
#define LIBMOPIA_DE_SAINT_VENANT_H

#include "OptProblemInterface.hpp"
#include <utopia.hpp>

#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/fe.h"
#include "libmesh/node_range.h"
#include "libmesh/parallel.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"

// Bring in everything from the libMesh namespace
using namespace libMesh;

// for 2D
#define LE_BOUNDARY_ID_MIN_X 3
#define LE_BOUNDARY_ID_MIN_Y 2
#define LE_BOUNDARY_ID_MAX_X 1
#define LE_BOUNDARY_ID_MAX_Y 0

// Bring in everything from the libMesh namespace
using namespace libMesh;

class DispBCSV : public FunctionBase<Number> {
public:
  DispBCSV(unsigned int u_var) : _u_var(u_var) { this->_initialized = true; }

  virtual Number operator()(const Point &, const Real = 0) {
    libmesh_not_implemented();
  }

  virtual void operator()(const Point &p, const Real,
                          DenseVector<Number> &output) {
    output.resize(1);
    output.zero();
    // const Real y = p(1);
    // const Real x = p(0);
    // Set the parabolic inflow boundary conditions at stations 0 & 1

    output(_u_var) = 1e-5;
  }

  virtual std::unique_ptr<FunctionBase<Number>> clone() const {
    return libmesh_make_unique<DispBCSV>(_u_var);
  }

private:
  const unsigned int _u_var;
};

template <class Matrix, class Vector>
class DeSaintVenant final : public utopia::OptProblemInterface<Matrix, Vector> {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

public:
  DeSaintVenant(MeshBase &mesh, const Number &young_modulus = 10,
                const Number &poisson_ratio = 0.3,
                const Number &forcing_magnitude = 0.0)
      : utopia::OptProblemInterface<Matrix, Vector>(mesh, "DeSaintVenant"),
        u_var_(this->system_.add_variable(
            "disp_x", Utility::string_to_enum<Order>("FIRST"))),
        v_var_(this->system_.add_variable(
            "disp_y", Utility::string_to_enum<Order>("FIRST"))),
        _young_modulus(young_modulus), _poisson_ratio(poisson_ratio),
        _forcing_magnitude(forcing_magnitude) {
    this->init();
  }

  void add_bc() override {
    std::set<boundary_id_type> clamped_boundaries;
    clamped_boundaries.insert(3);

    std::vector<unsigned int> variables;
    variables.push_back(u_var_);
    variables.push_back(v_var_);

    ZeroFunction<Number> zero;

    this->system_.get_dof_map().add_dirichlet_boundary((DirichletBoundary(
        clamped_boundaries, variables, zero, LOCAL_VARIABLE_ORDER)));

    ////////////////////////////////////////////////////////////////////////////////////////
    DispBCSV forceBC(u_var_);

    std::vector<unsigned int> variables_force;
    variables_force.push_back(u_var_);

    std::set<boundary_id_type> force_boundaries;
    force_boundaries.insert(LE_BOUNDARY_ID_MAX_X);

    this->system_.get_dof_map().add_dirichlet_boundary(
        (DirichletBoundary(force_boundaries, variables_force, &forceBC)));
  }

  void residual(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::NumericVector<libMesh::Number> &residual,
                libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    residual.zero();

    const Number mu = _young_modulus / (2. + (2. * _poisson_ratio));
    const Number lambda =
        (_young_modulus * _poisson_ratio) /
        ((1. + _poisson_ratio) * (1. - (2. * _poisson_ratio)));

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const unsigned int u_var = system.variable_number("disp_x");

    const DofMap &dof_map = system.get_dof_map();

    FEType fe_type = dof_map.variable_type(u_var);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    DenseVector<Number> Re;
    DenseSubVector<Number> Re_var[2] = {DenseSubVector<Number>(Re),
                                        DenseSubVector<Number>(Re)};

    std::vector<dof_id_type> dof_indices;
    std::vector<std::vector<dof_id_type>> dof_indices_var(dim);

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      for (unsigned int var = 0; var < dim; var++) {
        dof_map.dof_indices(elem, dof_indices_var[var], var);
      }

      const unsigned int n_dofs = dof_indices.size();
      const unsigned int n_var_dofs = dof_indices_var[0].size();

      fe->reinit(elem);

      Re.resize(n_dofs);
      for (unsigned int var = 0; var < dim; var++) {
        Re_var[var].reposition(var * n_var_dofs, n_var_dofs);
      }

      // qp -------------------
      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        DenseVector<Number> u_vec(dim);
        DenseMatrix<Number> grad_u(dim, dim);

        for (unsigned int var_i = 0; var_i < dim; var_i++) {
          for (unsigned int j = 0; j < n_var_dofs; j++) {
            u_vec(var_i) += phi[j][qp] * soln(dof_indices_var[var_i][j]);
          }

          // Row is variable u, v, or w column is x, y, or z
          for (unsigned int var_j = 0; var_j < dim; var_j++) {
            for (unsigned int j = 0; j < n_var_dofs; j++) {
              grad_u(var_i, var_j) +=
                  dphi[j][qp](var_j) * soln(dof_indices_var[var_i][j]);
            }
          }
        }

        DenseMatrix<Number> strain_tensor(dim, dim);
        for (unsigned int i = 0; i < dim; i++) {
          for (unsigned int j = 0; j < dim; j++) {
            strain_tensor(i, j) += 0.5 * (grad_u(i, j) + grad_u(j, i));

            for (unsigned int k = 0; k < dim; k++) {
              strain_tensor(i, j) += 0.5 * (grad_u(k, i) * grad_u(k, j));
            }
          }
        }

        // Define the deformation gradient
        DenseMatrix<Number> deformation_Grad(dim, dim);
        deformation_Grad = grad_u;
        // F = gard_u + I
        // as this is linear elasitcity F = I;
        for (unsigned int var = 0; var < dim; var++) {
          deformation_Grad(var, var) += 1.00;
        }

        // deformation_Grad.print(std::cout);
        // exit(0);

        Number trace_Strain = 0.0;
        for (unsigned int i = 0; i < dim; i++) {
          trace_Strain += strain_tensor(i, i);
        }

        DenseMatrix<Number> stress_tensor(dim, dim);
        for (unsigned int i = 0; i < dim; i++) {
          for (unsigned int j = 0; j < dim; j++) {
            stress_tensor(i, j) += (2. * mu * strain_tensor(i, j));
          }
          stress_tensor(i, i) += (lambda * trace_Strain);
        }

        // strain_tensor.print(std::cout);
        // std::cout << "-------- trace_Strain: " << trace_Strain << "   \n";
        // stress_tensor.print(std::cout);

        // std::cout << "mu: " << mu << "  lambda: " << lambda << "  \n";

        // exit(0);
        // stress_tensor.print(std::cout);

        // DenseVector<Number> f_vec(2);
        // f_vec(0) = 0.;
        // f_vec(1) = -_forcing_magnitude;

        for (unsigned int dof_i = 0; dof_i < n_var_dofs; dof_i++) {
          for (unsigned int i = 0; i < dim; i++) {
            for (unsigned int j = 0; j < dim; j++) {
              Number FxS_ij = 0.0;
              for (unsigned int m = 0; m < dim; m++) {
                FxS_ij += deformation_Grad(i, m) * stress_tensor(m, j);
              }

              Re_var[i](dof_i) += JxW[qp] * (FxS_ij * dphi[dof_i][qp](j));

              // Re_var[i].print(std::cout);
              // exit(0);
            }

            // Re_var[i](dof_i) += JxW[qp] * (-f_vec(i) * phi[dof_i][qp]);
          }
        }
      }

      // Re_var[0].print(std::cout);
      // exit(0);

      dof_map.constrain_element_vector(Re, dof_indices);
      residual.add_vector(Re, dof_indices);
    } // end of qp
  }

  /**
   * Function which computes the jacobian.
   */
  void jacobian(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::SparseMatrix<libMesh::Number> &jacobian,
                libMesh::NonlinearImplicitSystem &sys) override {
    std::cout << "--------- empty ---------- \n";
  }

  void libmesh_energy(const libMesh::NumericVector<libMesh::Number> &soln,
                      libMesh::Number &energy,
                      libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    const Number mu = _young_modulus / (2. + (2. * _poisson_ratio));
    const Number lambda =
        (_young_modulus * _poisson_ratio) /
        ((1. + _poisson_ratio) * (1. - (2. * _poisson_ratio)));

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const unsigned int u_var = system.variable_number("disp_x");

    this->init();
    const DofMap &dof_map = system.get_dof_map();

    FEType fe_type = dof_map.variable_type(u_var);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    std::unique_ptr<FEBase> fe_face(FEBase::build(dim, fe_type));
    QGauss qface(dim - 1, fe_type.default_quadrature_order());
    fe_face->attach_quadrature_rule(&qface);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    std::vector<dof_id_type> dof_indices;
    std::vector<std::vector<dof_id_type>> dof_indices_var(dim);

    DenseVector<Number> Fe;
    DenseSubVector<Number> Fe_var[2] = {DenseSubVector<Number>(Fe),
                                        DenseSubVector<Number>(Fe)};

    std::unique_ptr<NumericVector<Number>> force = soln.zero_clone();
    force->zero();

    energy = 0.0;

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      for (unsigned int var = 0; var < dim; var++) {
        dof_map.dof_indices(elem, dof_indices_var[var], var);
      }

      const unsigned int n_dofs = dof_indices.size();
      const unsigned int n_var_dofs = dof_indices_var[0].size();

      fe->reinit(elem);

      Fe.resize(n_dofs);

      for (unsigned int var = 0; var < dim; var++) {
        Fe_var[var].reposition(var * n_var_dofs, n_var_dofs);
      }

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        DenseVector<Number> u_vec(dim);
        DenseMatrix<Number> grad_u(dim, dim);

        for (unsigned int var_i = 0; var_i < dim; var_i++) {
          for (unsigned int j = 0; j < n_var_dofs; j++) {
            u_vec(var_i) += phi[j][qp] * soln(dof_indices_var[var_i][j]);
          }

          // Row is variable u, v, or w column is x, y, or z
          for (unsigned int var_j = 0; var_j < dim; var_j++) {
            for (unsigned int j = 0; j < n_var_dofs; j++) {
              grad_u(var_i, var_j) +=
                  dphi[j][qp](var_j) * soln(dof_indices_var[var_i][j]);
            }
          }
        }

        DenseMatrix<Number> strain_tensor(dim, dim);
        for (unsigned int i = 0; i < dim; i++) {
          for (unsigned int j = 0; j < dim; j++) {
            strain_tensor(i, j) += 0.5 * (grad_u(i, j) + grad_u(j, i));
          }
        }

        // Define the deformation gradient
        DenseMatrix<Number> deformation_Grad(dim, dim);
        // F = grad_u;
        // F = gard_u + I
        // as this is linear elasitcity F = I;
        for (unsigned int var = 0; var < dim; var++) {
          deformation_Grad(var, var) = 1.00;
        }

        DenseMatrix<Number> StrainStrain(dim, dim);
        for (unsigned int i = 0; i < dim; i++) {
          for (unsigned int j = 0; j < dim; j++) {
            StrainStrain(i, j) += strain_tensor(i, j) * strain_tensor(j, i);
          }
        }

        Number trace_Strain = 0.0;
        Number trace_StrainStrain = 0.0;
        for (unsigned int i = 0; i < dim; i++) {
          trace_Strain += strain_tensor(i, i);
          trace_StrainStrain += StrainStrain(i, i);
        }

        energy += JxW[qp] * ((mu * trace_StrainStrain) +
                             ((lambda / 2.0) * trace_Strain * trace_Strain));

        DenseVector<Number> f_vec(2);
        f_vec(0) = 0.0;
        f_vec(1) = -_forcing_magnitude;

        for (unsigned int dof_i = 0; dof_i < n_var_dofs; dof_i++) {
          for (unsigned int i = 0; i < dim; i++) {
            Fe_var[i](dof_i) += JxW[qp] * (-f_vec(i) * phi[dof_i][qp]);
          }
        }
      }
      force->add_vector(Fe, dof_indices);
    }
    energy -= force->dot(soln);
  }

private:
  Number _young_modulus;
  Number _poisson_ratio;
  Number _forcing_magnitude;

  unsigned int u_var_;
  unsigned int v_var_;
};

#endif