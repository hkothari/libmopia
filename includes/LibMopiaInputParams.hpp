#ifndef LIBMOPIA_INPUT_PARAMS_HPP
#define LIBMOPIA_INPUT_PARAMS_HPP

#include <libmesh/getpot.h>

using namespace libMesh;

class LibMopiaInputParameters {
public:
  LibMopiaInputParameters() {}
  ~LibMopiaInputParameters() {}

  bool readParameters(GetPot &infile) {
    dim_ = infile("dim", 2);
    n_elem_x_ = infile("n_elem_x", 10);
    n_elem_y_ = infile("n_elem_y", 10);
    n_elem_z_ = infile("n_elem_z", 10);
    n_levels_ = infile("n_levels", 3);
    n_vars_ = infile("n_vars", 1);

    x_min_ = infile("x_min", 0.0);
    x_max_ = infile("x_max", 1.0);
    y_min_ = infile("y_min", 0.0);
    y_max_ = infile("y_max", 1.0);
    z_min_ = infile("z_min", 0.0);
    z_max_ = infile("z_max", 1.0);

    lambda_ = infile("lambda", 5.0);

    young_modulus_ = infile("young_modulus", 100);
    poisson_ratio_ = infile("poisson_ratio", 0.3);
    forcing_magnitude_ = infile("forcing_magnitude", 1e-4);

    generated_mesh_ = infile("generated_mesh", true);
    file_mesh_ = infile("file_mesh", true);
    mesh_path_ = infile("mesh_path", "my_mesh");
    log_output_path_ = infile("log_output_path", "log_output_path.csv");
    ls_solver_type_ = infile("ls_solver_type", "CG-MG");

    use_restriction_ = infile("use_restriction", false);
    neumann_magnitude_ = infile("neumann_magnitude", 1e-4);

    return true;
  }

  int dim() const { return dim_; }
  int n_elem_x() const { return n_elem_x_; }
  int n_elem_y() const { return n_elem_y_; }
  int n_elem_z() const { return n_elem_z_; }
  int n_levels() const { return n_levels_; }
  int n_vars() const { return n_vars_; }

  double x_min() const { return x_min_; }
  double x_max() const { return x_max_; }
  double y_min() const { return y_min_; }
  double y_max() const { return y_max_; }
  double z_min() const { return z_min_; }
  double z_max() const { return z_max_; }

  bool generated_mesh() const { return generated_mesh_; }
  bool file_mesh() const { return file_mesh_; }
  std::string mesh_path() const { return mesh_path_; }
  std::string log_output_path() const { return log_output_path_; }

  // Bratu params
  double lambda() const { return lambda_; }

  // Neohook params
  double young_modulus() const { return young_modulus_; }
  double poisson_ratio() const { return poisson_ratio_; }
  double forcing_magnitude() const { return forcing_magnitude_; }

  std::string ls_solver_type() const { return ls_solver_type_; }

  bool use_restriction() const { return use_restriction_; }
  double neumann_magnitude() const { return neumann_magnitude_; }

private:
  int dim_;
  int n_elem_x_;
  int n_elem_y_;
  int n_elem_z_;
  int n_levels_;
  int n_vars_;

  double x_min_;
  double x_max_;
  double y_min_;
  double y_max_;
  double z_min_;
  double z_max_;

  double lambda_;

  double young_modulus_;
  double poisson_ratio_;
  double forcing_magnitude_;

  bool generated_mesh_;
  bool file_mesh_;
  std::string mesh_path_;
  std::string log_output_path_;

  std::string ls_solver_type_;

  bool use_restriction_;
  double neumann_magnitude_;
};
#endif