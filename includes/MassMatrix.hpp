#include <libmesh/explicit_system.h>
#include <libmesh/implicit_system.h>

#include <utopia.hpp>

#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/fe.h"
#include "libmesh/getpot.h"
#include "libmesh/gmv_io.h"
#include "libmesh/libmesh.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/linear_solver.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/node_range.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/optimization_solver.h"
#include "libmesh/optimization_system.h"
#include "libmesh/parallel.h"
#include "libmesh/parallel_mesh.h"
#include "libmesh/petsc_linear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_vector.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/solver_configuration.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/string_to_enum.h"
#include "libmesh/transient_system.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"

// Bring in everything from the libMesh namespace
using namespace libMesh;

/**
 * This class encapsulate all functionality required for assembling
 * the objective function, gradient, and hessian.
 */

// assemble mass matrix - this could be done more efficiently
void assemble_mass_matrix(EquationSystems &es_,
                          const std::string &libmesh_dbg_var(system_name)) {
  const MeshBase &mesh = es_.get_mesh();
  ImplicitSystem &system = es_.get_system<ImplicitSystem>("MassMatrix");

  const unsigned int dim = mesh.mesh_dimension();
  // const unsigned int u_var = es_.variable_number ("u");

  const DofMap &dof_map = system.get_dof_map();
  FEType fe_type = dof_map.variable_type(0);
  std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
  QGauss qrule(dim, fe_type.default_quadrature_order());
  fe->attach_quadrature_rule(&qrule);

  const std::vector<Real> &JxW = fe->get_JxW();
  const std::vector<std::vector<Real>> &phi = fe->get_phi();
  const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

  std::vector<dof_id_type> dof_indices;

  DenseMatrix<Number> Me;

  SparseMatrix<Number> &mass = system.get_system_matrix();
  // mass.zero();

  for (const auto &elem : mesh.active_local_element_ptr_range()) {
    dof_map.dof_indices(elem, dof_indices);

    const unsigned int n_dofs = dof_indices.size();

    fe->reinit(elem);

    Me.resize(n_dofs, n_dofs);

    for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
      for (unsigned int dof_i = 0; dof_i < n_dofs; dof_i++) {
        for (unsigned int dof_j = 0; dof_j < n_dofs; dof_j++) {
          Me(dof_i, dof_j) += JxW[qp] * (phi[dof_i][qp] * phi[dof_j][qp]);
        }
      }

      // This will zero entries of Re corresponding to Dirichlet dofs.
      mass.add_matrix(Me, dof_indices);
    }
  }
}

template <class Matrix, class Vector>
class MassMatrix {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

 public:
  MassMatrix(EquationSystems &equation_systems)
      : es_(equation_systems),
        system_(
            equation_systems.add_system<LinearImplicitSystem>("MassMatrix")),
        u_var_(system_.add_variable("u",
                                    Utility::string_to_enum<Order>("FIRST"))) {
    // ------------------------------------------------------------

    // std::cout << "end of MassMatrix constructor" << std::endl;
    system_.attach_assemble_function(assemble_mass_matrix);
    es_.init();
  }

  // computing the mass matrix
  bool get_mass_matrix(Matrix &mass) const {
    // the biggest hack of the century -  can be redone
    system_.solve();

    SparseMatrix<Number> &matrix_libmesh = system_.get_system_matrix();

    Mat p_mat =
        cast_ptr<libMesh::PetscMatrix<Number> *>(&matrix_libmesh)->mat();

    utopia::convert(p_mat, mass);

    return true;
  }

  DofMap &get_dof_map() { return system_.get_dof_map(); }

  MeshBase &get_mesh() { return es_.get_mesh(); }

 private:
  /**
   * Keep a reference to the EquationSystems.
   */
  EquationSystems &es_;
  ImplicitSystem &system_;

  unsigned int u_var_;
};

// }
