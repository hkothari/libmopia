#ifndef LIBMOPIA_MIN_SURFACE_H
#define LIBMOPIA_MIN_SURFACE_H

#include "libmesh/nonlinear_implicit_system.h"
#include "libmesh/nonlinear_solver.h"
#include <libmesh/explicit_system.h>
#include <libmesh/implicit_system.h>

#include <utopia.hpp>

#include "OptProblemInterface.hpp"
#include "libmesh/boundary_info.h"
#include "libmesh/dense_matrix.h"
#include "libmesh/dense_vector.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/dof_map.h"
#include "libmesh/elem.h"
#include "libmesh/elem_range.h"
#include "libmesh/equation_systems.h"
#include "libmesh/exodusII_io.h"
#include "libmesh/fe.h"
#include "libmesh/getpot.h"
#include "libmesh/gmv_io.h"
#include "libmesh/libmesh.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/linear_solver.h"
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"
#include "libmesh/mesh_refinement.h"
#include "libmesh/node_range.h"
#include "libmesh/numeric_vector.h"
#include "libmesh/optimization_solver.h"
#include "libmesh/optimization_system.h"
#include "libmesh/parallel.h"
#include "libmesh/parallel_mesh.h"
#include "libmesh/petsc_linear_solver.h"
#include "libmesh/petsc_matrix.h"
#include "libmesh/petsc_vector.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/solver_configuration.h"
#include "libmesh/sparse_matrix.h"
#include "libmesh/string_to_enum.h"
#include "libmesh/transient_system.h"
#include "libmesh/vector_value.h"
#include "libmesh/zero_function.h"

// for 2D
#define MIN_BOUNDARY_ID_MIN_X 0
#define MIN_BOUNDARY_ID_MIN_Y 3
#define MIN_BOUNDARY_ID_MAX_X 2
#define MIN_BOUNDARY_ID_MAX_Y 1

// Bring in everything from the libMesh namespace
using namespace libMesh;

class ClampedFun : public FunctionBase<Number> {
public:
  ClampedFun(unsigned int u_var) : _u_var(u_var) { this->_initialized = true; }

  virtual Number operator()(const Point &, const Real = 0) {
    libmesh_not_implemented();
  }

  virtual void operator()(const Point &p, const Real,
                          DenseVector<Number> &output) {
    output.resize(1);
    output.zero();
    const Real y = p(1);
    // const Real x = p(0);
    // Set the parabolic inflow boundary conditions at stations 0 & 1

    output(_u_var) = y * (1.0 - y);
  }

  virtual std::unique_ptr<FunctionBase<Number>> clone() const {
    return libmesh_make_unique<ClampedFun>(_u_var);
  }

private:
  const unsigned int _u_var;
};

class AntiClampledFun : public FunctionBase<Number> {
public:
  AntiClampledFun(unsigned int u_var) : _u_var(u_var) {
    this->_initialized = true;
  }

  virtual Number operator()(const Point &, const Real = 0) {
    libmesh_not_implemented();
  }

  virtual void operator()(const Point &p, const Real,
                          DenseVector<Number> &output) {
    output.resize(1);
    output.zero();
    // const Real y = p(1);
    const Real x = p(0);
    // Set the parabolic inflow boundary conditions at stations 0 & 1

    // output(_u_var) = x * (x - 1.0);
    output(_u_var) = x * (1.0 - x);
  }

  virtual std::unique_ptr<FunctionBase<Number>> clone() const {
    return libmesh_make_unique<AntiClampledFun>(_u_var);
  }

private:
  const unsigned int _u_var;
};

template <class Matrix, class Vector>
class MinimalSurface final
    : public utopia::OptProblemInterface<Matrix, Vector> {
  typedef typename utopia::Traits<Vector>::Scalar Scalar;
  typedef typename utopia::Traits<Vector>::SizeType SizeType;

public:
  MinimalSurface(MeshBase &mesh)
      : utopia::OptProblemInterface<Matrix, Vector>(mesh, "MinimalSurface"),
        u_var_(this->system_.add_variable(
            "u", Utility::string_to_enum<Order>("FIRST"))) {
    this->init();
  }

  void add_bc() override {
    std::set<boundary_id_type> clamped_boundaries;
    clamped_boundaries.insert(MIN_BOUNDARY_ID_MIN_Y);
    clamped_boundaries.insert(MIN_BOUNDARY_ID_MAX_Y);

    std::set<boundary_id_type> anti_clamped_bc;
    anti_clamped_bc.insert(MIN_BOUNDARY_ID_MIN_X);
    anti_clamped_bc.insert(MIN_BOUNDARY_ID_MAX_X);

    std::vector<unsigned int> variables;
    variables.push_back(u_var_);

    AntiClampledFun anti_bc(u_var_);

    this->system_.get_dof_map().add_dirichlet_boundary(
        (DirichletBoundary(anti_clamped_bc, variables, &anti_bc)));

    ZeroFunction<> zf;

    // Most DirichletBoundary users will want to supply a "locally
    DirichletBoundary dirichlet_bc_fixed(clamped_boundaries, variables, zf,
                                         LOCAL_VARIABLE_ORDER);

    this->system_.get_dof_map().add_dirichlet_boundary(dirichlet_bc_fixed);
    this->system_.get_dof_map().add_dirichlet_boundary(
        (DirichletBoundary(anti_clamped_bc, variables, &anti_bc)));
  }

  void residual(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::NumericVector<libMesh::Number> &residual,
                libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const DofMap &dof_map = system.get_dof_map();
    FEType fe_type = dof_map.variable_type(0);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    std::vector<dof_id_type> dof_indices;

    DenseVector<Number> Re;

    residual.zero();

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      const unsigned int n_dofs = dof_indices.size();

      fe->reinit(elem);

      Re.resize(n_dofs);

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        Number u = 0;
        Gradient grad_u;
        for (unsigned int j = 0; j < n_dofs; j++) {
          u += phi[j][qp] * soln(dof_indices[j]);
          grad_u += dphi[j][qp] * soln(dof_indices[j]);
        }
        const Number coeff = 1.0 / std::sqrt(1.0 + std::abs(grad_u * grad_u));

        for (unsigned int i = 0; i < n_dofs; i++) {
          Re(i) += JxW[qp] * (coeff * (dphi[i][qp] * grad_u));
        }
      }

      // This will zero entries of Re corresponding to Dirichlet dofs.
      dof_map.constrain_element_vector(Re, dof_indices);
      residual.add_vector(Re, dof_indices);
    }
  }

  /**
   * Function which computes the jacobian.
   */
  void jacobian(const libMesh::NumericVector<libMesh::Number> &soln,
                libMesh::SparseMatrix<libMesh::Number> &jacobian,
                libMesh::NonlinearImplicitSystem &sys) override {
    std::cout << "--------- empty ---------- \n";
  }

  void libmesh_energy(const libMesh::NumericVector<libMesh::Number> &soln,
                      libMesh::Number &energy,
                      libMesh::NonlinearImplicitSystem &sys) override {
    EquationSystems &es = sys.get_equation_systems();

    // Get a constant reference to the mesh object.
    const MeshBase &mesh = es.get_mesh();

    // The dimension that we are running
    const unsigned int dim = mesh.mesh_dimension();

    // Get a reference to the NonlinearImplicitSystem we are solving
    NonlinearImplicitSystem &system = es.get_system<NonlinearImplicitSystem>(0);

    const DofMap &dof_map = system.get_dof_map();
    FEType fe_type = dof_map.variable_type(0);
    std::unique_ptr<FEBase> fe(FEBase::build(dim, fe_type));
    QGauss qrule(dim, fe_type.default_quadrature_order());
    fe->attach_quadrature_rule(&qrule);

    const std::vector<Real> &JxW = fe->get_JxW();
    const std::vector<std::vector<Real>> &phi = fe->get_phi();
    const std::vector<std::vector<RealGradient>> &dphi = fe->get_dphi();

    std::vector<dof_id_type> dof_indices;

    energy = 0.0;

    for (const auto &elem : mesh.active_local_element_ptr_range()) {
      dof_map.dof_indices(elem, dof_indices);

      const unsigned int n_dofs = dof_indices.size();

      fe->reinit(elem);

      for (unsigned int qp = 0; qp < qrule.n_points(); qp++) {
        Number u = 0;
        Gradient grad_u;
        for (unsigned int j = 0; j < n_dofs; j++) {
          u += phi[j][qp] * soln(dof_indices[j]);
          grad_u += dphi[j][qp] * soln(dof_indices[j]);
        }
        energy += JxW[qp] * (std::sqrt(1.0 + std::abs(grad_u * grad_u)));
      }
    }
  }

private:
  unsigned int u_var_;
};

#endif